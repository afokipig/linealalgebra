#pragma once

#include "Types/Vector2D.h"
#include "Types/Vector3D.h"

float AngleBetweenVectors(const Vector2D& start, const Vector2D& end);