#pragma once


class Vector2D
{
public:

	float X;
	float Y;

	constexpr explicit Vector2D();

	constexpr explicit Vector2D(float x, float y);

	//////////////////////////////////////////////////////
	// Symple operators

	Vector2D&& operator+(const Vector2D& other) const;

	Vector2D&& operator-(const Vector2D& other) const;

	Vector2D&& operator*(float number) const;

	Vector2D&& operator/(float number) const;

	const Vector2D& operator+=(const Vector2D& other);

	const Vector2D& operator-=(const Vector2D& other);

	const Vector2D& operator*=(float number);

	const Vector2D& operator/=(float number);

	//////////////////////////////////////////////////////
	// Hard operators

	float operator*(const Vector2D& other) const;

	//////////////////////////////////////////////////////
	// Methods

	float Length();

	Vector2D&& Normalized();

	float DistanceTo(const Vector2D& dest) const;

	const Vector2D& Rotate(float angle);

};

