#pragma once


class Vector3D
{

	// Constructors and destructor
public:

	constexpr explicit Vector3D() noexcept;

	constexpr explicit Vector3D(float x, float y, float z) noexcept;

	constexpr explicit Vector3D(const Vector3D& other) noexcept;

	constexpr explicit Vector3D(Vector3D&& other) noexcept;

	constexpr explicit Vector3D(float value) noexcept;

	//*******************************************************************************************************************************************



	// Public methods
public:

	// Operators

	Vector3D&& operator+(const Vector3D& other) const;

	Vector3D& operator+=(const Vector3D& other);

	Vector3D&& operator-(const Vector3D& other) const;

	Vector3D& operator-=(const Vector3D& other);

	Vector3D&& operator*(float value) const;

	Vector3D& operator*=(float value);

	Vector3D&& operator/(float value) const;

	Vector3D& operator/=(float value);

	Vector3D&& operator*(const Vector3D& other) const;

	Vector3D& operator*=(const Vector3D& other);

	// Math methods

	float Length() const;
	
	/*
	Doesn't modify vector, return normalized value
	*/
	Vector3D&& Normals() const;

	/*
	Modifies vector, normalizes it
	*/
	Vector3D& Normalize();

	//*******************************************************************************************************************************************



	// Protected methods
protected:

	//*******************************************************************************************************************************************



	// Private methods
private:

	//*******************************************************************************************************************************************



	// Public variables
public:

	float X;

	float Y;

	float Z;

	//*******************************************************************************************************************************************



	// Protected variables
protected:

	//*******************************************************************************************************************************************



	// Private variables
private:

};
