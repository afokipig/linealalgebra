﻿
#include <iostream>

#include "Types/Vector2D.h"
#include "Types/Vector3D.h"

int main()
{


	Vector3D Pos1{ 1, 1, 0 };
	Vector3D Pos2{ 3, 3, 0 };

	Vector3D Dir1{ 3, 1, 3 };
	Vector3D Vert1{ 1, 3, 2 };
	Vector3D Dir2{ 3, 3, 0 };

	Vector2D test1{ 0, 2 };


	std::cout << Dir1.X << '\t' << Dir1.Y << '\t' << Dir1.Z << std::endl;

	std::cout << Vert1.X << '\t' << Vert1.Y << '\t' << Vert1.Z << std::endl;

	std::cout << (Vert1 * Dir1).X << '\t' << (Vert1 * Dir1).Y << '\t' << (Vert1 * Dir1).Z << std::endl;

	std::cout << (Dir1 * Vert1).X << '\t' << (Dir1 * Vert1).Y << '\t' << (Dir1 * Vert1).Z << std::endl;

	std::cout << Vert1.X << '\t' << Vert1.Y << '\t' << Vert1.Z << std::endl;

	std::cout << Vert1.Length() << std::endl;

	Vert1.Normalize();

	std::cout << Vert1.X << '\t' << Vert1.Y << '\t' << Vert1.Z << std::endl;

	std::cout << Vert1.Length() << std::endl;

	std::cout << test1.X << '\t' << test1.Y << std::endl;

	std::cout << test1.Rotate(49).X << '\t' << test1.Y << std::endl;

	// Использование объекта, как массив)))))
	float* ptr = &Pos1.X;

	std::cout << ptr[0] << '\t' << ptr[1] << '\t' << ptr[2] << std::endl;

	ptr = &Vert1.X;

	std::cout << ptr[0] << '\t' << ptr[1] << '\t' << ptr[2] << std::endl;

	ptr = &Dir2.X;

	std::cout << ptr[0] << '\t' << ptr[1] << '\t' << ptr[2] << std::endl;
}

