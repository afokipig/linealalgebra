#include "MathFunctions.h"

#include <cmath>
#include <utility>

static const float PI{ acosf(-1.0) };

float AngleBetweenVectors(const Vector2D& start, const Vector2D& end)
{
	return acosf(start * end) * 180.0 / PI;
}
//*******************************************************************************************************************************************