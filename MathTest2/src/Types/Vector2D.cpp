
#include "Types/Vector2D.h"

#include <utility>
#include <iostream>
#include <cmath>


static const float PI{ acosf(-1.0) };


constexpr Vector2D::Vector2D()
	: X{ 0.f }
	, Y{ 0.f }
{}
//*******************************************************************************************************************************************



constexpr Vector2D::Vector2D(float x, float y)
	: X{ x }
	, Y{ y }
{}
//*******************************************************************************************************************************************



Vector2D&& Vector2D::operator+(const Vector2D& other) const
{
	Vector2D result{ (X + other.X), (Y + other.Y) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector2D&& Vector2D::operator-(const Vector2D& other) const
{
	Vector2D result{ (X - other.X), (Y - other.Y) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector2D&& Vector2D::operator*(float number) const
{
	Vector2D result{ (X * number), (Y * number) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector2D&& Vector2D::operator/(float number) const
{
	Vector2D result{ (X / number), (Y / number) };

	return std::move(result);
}
//*******************************************************************************************************************************************



const Vector2D& Vector2D::operator+=(const Vector2D& other)
{
	X += other.X;
	Y += other.Y;

	return *this;
}
//*******************************************************************************************************************************************



const Vector2D& Vector2D::operator-=(const Vector2D& other)
{
	X -= other.X;
	Y -= other.Y;

	return *this;
}
//*******************************************************************************************************************************************



const Vector2D& Vector2D::operator*=(float number)
{
	X *= number;
	Y *= number;

	return *this;
}
//*******************************************************************************************************************************************



const Vector2D& Vector2D::operator/=(float number)
{
	X /= number;
	Y /= number;

	return *this;
}
//*******************************************************************************************************************************************



float Vector2D::operator*(const Vector2D& other) const
{
	return (X * other.X) + (Y * other.Y);
}
//*******************************************************************************************************************************************



float Vector2D::Length()
{
	return sqrtf((X * X) + (Y * Y));
}
//*******************************************************************************************************************************************



Vector2D&& Vector2D::Normalized()
{
	Vector2D result{ (X / Length()), (Y / Length()) };

	return std::move(result);
}
//*******************************************************************************************************************************************



float Vector2D::DistanceTo(const Vector2D& dest) const
{
	return (*this - dest).Length();
}
//*******************************************************************************************************************************************



const Vector2D& Vector2D::Rotate(float angle)
{

	angle = angle / 180 * PI;

	float tmpX{ X * cos(angle) - Y * sin(angle) };
	float tmpY{ X * sin(angle) + Y * cos(angle) };
	
	X = tmpX;
	Y = tmpY;

	return *this;
}
//*******************************************************************************************************************************************



float Distance(const Vector2D& start, const Vector2D& end)
{
	Vector2D direction = end - start;

	return direction.Length();
}
//*******************************************************************************************************************************************



Vector2D&& SubstractionVector2D(const Vector2D& vec1, const Vector2D& vec2)
{
	Vector2D result{ (vec1.X - vec2.X), (vec1.Y - vec2.Y) };

	return std::move(result);
}
//*******************************************************************************************************************************************