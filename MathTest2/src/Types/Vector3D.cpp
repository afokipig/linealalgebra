#include "Vector3D.h"

#include <utility>



constexpr Vector3D::Vector3D() noexcept
	: X{ 0 }
	, Y{ 0 }
	, Z{ 0 }
{}
//*******************************************************************************************************************************************



constexpr Vector3D::Vector3D(float x, float y, float z) noexcept
	: X{ x }
	, Y{ y }
	, Z{ z }
{}
//*******************************************************************************************************************************************



constexpr Vector3D::Vector3D(const Vector3D& other) noexcept
	: X{ other.X }
	, Y{ other.Y }
	, Z{ other.Z }
{}
//*******************************************************************************************************************************************



constexpr Vector3D::Vector3D(Vector3D&& other) noexcept
	: X{ other.X }
	, Y{ other.Y }
	, Z{ other.Z }
{}
//*******************************************************************************************************************************************



constexpr Vector3D::Vector3D(float value) noexcept
	: X{ value }
	, Y{ value }
	, Z{ value }
{}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::operator+(const Vector3D& other) const
{
	Vector3D result{ (X + other.X), (Y + other.Y), (Z + other.Z) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::operator+=(const Vector3D& other)
{
	X += other.X;
	Y += other.Y;
	Z += other.Z;

	return *this;
}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::operator-(const Vector3D& other) const
{
	Vector3D result{ (X - other.X), (Y - other.Y), (Z - other.Z) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::operator-=(const Vector3D& other)
{
	X -= other.X;
	Y -= other.Y;
	Z -= other.Z;

	return *this;
}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::operator*(float value) const
{
	Vector3D result{ (X * value), (Y * value), (Z * value) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::operator*=(float value)
{
	X *= value;
	Y *= value;
	Z *= value;

	return *this;
}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::operator/(float value) const
{
	Vector3D result{ (X / value), (Y / value), (Z / value) };

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::operator/=(float value)
{
	X /= value;
	Y /= value;
	Z /= value;

	return *this;
}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::operator*(const Vector3D& other) const
{
	Vector3D result
	{
		((Y * other.Z) - (Z * other.Y)),
		((Z * other.X) - (X * other.Z)),
		((X * other.Y) - (Y * other.X))
	};

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::operator*=(const Vector3D& other)
{
	float tmpX = (Y * other.Z) - (Z * other.Y);
	float tmpY = (Z * other.X) - (X * other.Z);
	float tmpZ = (X * other.Y) - (Y * other.X);

	X = tmpX;
	Y = tmpY;
	Z = tmpZ;

	return *this;
}
//*******************************************************************************************************************************************



float Vector3D::Length() const
{
	return sqrtf( (X * X) + (Y * Y) + (Z * Z) );
}
//*******************************************************************************************************************************************



Vector3D&& Vector3D::Normals() const
{
	Vector3D result
	{
		X / Length(),
		Y / Length(),
		Z / Length()
	};

	return std::move(result);
}
//*******************************************************************************************************************************************



Vector3D& Vector3D::Normalize()
{
	*this /= Length();

	return *this;
}
//*******************************************************************************************************************************************



